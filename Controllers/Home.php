<?php

require 'Main.php';
//require 'Models/Medicina.php';

class Home extends Main {

	public function __construct() {
		// Code
	}

	public function load() {
		$this->loadView('main');
	}

	public function testORM()
	{
		$Medicina = new Medicina();

		echo "TODA LA EXISTENCIA: (ANTES)";
		echo "<br>";
		echo $Medicina->findAll();

		echo "<br>";
		echo "SELECCION POR ID";
		echo "<br>";
		echo $Medicina->findColumn("Id");

		$Medicina->insert("1","Paracetamol");
		$Medicina->insert("2","Aspirina");
		$Medicina->insert("3","Cafiaspirina");
		$Medicina->insert("4","Tribe12");
		$Medicina->insert("5","Coca cola");
		$Medicina->insert("6","Bencenatodeaspirina");

		$Medicina->updateRow("Medicina","Cambio","id=\"1\" AND Medicina = \"Paracetamol\"");

		$Medicina->deleteRow("Id=\"2\"3\"4\"5");

		echo "<br><br>";
		echo "Select all: (Despues)";
		echo "<br>";
		echo $Medicina->findAll();

		echo "<br>";
		echo "Select specific (Despues)";
		echo "<br>";
		echo $Medicina->findColumn("Medicina");
	}
}
?>
